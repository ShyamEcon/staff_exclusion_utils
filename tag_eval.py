import cv2
import cv2 as cv
import pandas as pd
import argparse
import os
import numpy as np


def annotate(path,video_caputre):
    frame_count = int(video_caputre.get(cv2.CAP_PROP_FRAME_COUNT))
    name = path.split('/')[-1]
    if os.path.exists('/home/shyam/staff_exclusion/vid/tag_videos/directionals/tag_view_count.csv'):
        tagdf = pd.read_csv('/home/shyam/staff_exclusion/vid/tag_videos/directionals/tag_view_count.csv')
        tagdf = tagdf.iloc[:,1:]
        print('Read the existing DF')
        print(tagdf,tagdf.shape)
    else:
        df = pd.DataFrame(columns=['visible_frames','vstart_frame','vend_frame','total_frames','tstart_frame','tend_frame','video','folder'])
        df.to_csv('/home/shyam/staff_exclusion/vid/tag_videos/directionals/tag_view_count.csv')
        print('created DF')
    x = 30
    ld_dict = {}
    rd_dict = {}
    findic = {}
    findic['ld'] = {}
    findic['rd'] = {}
    lsw = 0
    rsw = 0

    while (video_caputre.isOpened()):

        # # capture a frame
        ret, frame = video_caputre.read()
        if ret == False:
            print("!!! Failed cap.read()")
            break
        
        status = ['visible','not visible','outside FOV']

        ld_status = status[lsw]
        # rd_status = status[rsw]

        i = video_caputre.get(cv2.CAP_PROP_POS_FRAMES)-1
        
        frame_copy = frame.copy()
        frame = cv2.putText(frame, "Frame number : {}".format(i), (400, 30),
                                cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 0, 0), 2)

        frame = cv2.putText(frame, "status : {}".format(ld_status), (80, 80),
                                cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,255,0 if (ld_status == "Closed") else 255), 2)
        # frame = cv2.putText(frame, "right door : {}".format(rd_status), (400, 80),
        #                         cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,255,0 if (rd_status == "Closed") else 255), 2)
        cv2.imshow('video', frame)

        # check if 'space' was pressed and wait for a 'left arrow' press
        # key = cv2.waitKey(int(frame_count/1000))

        key = cv2.waitKey(x)
        if (key & 0xFF == 43):
            x = (x // 2) if x > 1 else 1
            key = cv2.waitKey(x)
            print('playback speed : %.2f'%(30/x))

        if (key & 0xFF == 45):
            x = (x*2) if x < 1980 else 1980
            key = cv2.waitKey(x)
            
            print('playback speed : %.2f'%(30/x) )
         
        
        # key = cv2.waitKey(1)

        if (key & 0xFF == ord('e')):
            lsw = (lsw + 1) % 3
            print('e pressed ',lsw,status[lsw])
            
        # if (key & 0xFF == ord('r')):
        #     rsw = (rsw + 1) % 2
        #     print('r pressed',rsw,status[rsw])



        # if i% 5 == 0:
        #     if (i in findic['ld']) and (findic['rd']):
        #         pass
        #     else:
        findic['ld'].update({int(i) : ld_status})
            # findic['rd'].update({int(i) : rd_status})
        
        # last_detected = datetime.now()
        if (key & 0xFF == 32):
            # sleep here until a valid key is pressed
            while (True):

                if (key in [ord('e'),ord('a'),ord('d')]):
                        
                    i = video_caputre.get(cv2.CAP_PROP_POS_FRAMES)
                    # i = i - (i % 5)
                    # video_caputre.set(cv2.CAP_PROP_POS_FRAMES,i)
                    print(i)
                    _ , frame = video_caputre.read()
                    
                    frame = cv2.putText(frame, "Frame number : {}".format(i), (400, 30),
                                    cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 0, 0), 2)
                    # if (key & 0xFF == ord('a')):
                    #     # if i != 0:
                    #     lsw = status.index(findic['ld'][int(i)])
                    #     ld_status = status[lsw]
                    #     print(status[lsw])

                    #     # rsw = status.index(findic['rd'][int(i)])
                    #     # rd_status = status[rsw]
                    #     # print(status[rsw])

                    #     frame = cv2.putText(frame, "status : {}".format(ld_status), (80, 80),
                    #                 cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,255,0 if (ld_status == "Closed") else 255), 2)
                    #     # frame = cv2.putText(frame, "right door : {}".format(rd_status), (400, 80),
                    #     #             cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,255,0 if (rd_status == "Closed") else 255), 2)             
                    # elif (key & 0xFF == ord('d')):
                    #     frame = cv2.putText(frame, "status : {}".format(ld_status), (80, 80),
                    #                 cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,255,0 if (ld_status == "Closed") else 255), 2)
                        # frame = cv2.putText(frame, "right door : {}".format(rd_status), (400, 80),
                        #             cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,255,0 if (rd_status == "Closed") else 255), 2)
                    # else:
                    ld_status = status[lsw]
                    # rd_status = status[rsw]
                    findic['ld'][int(i)] = ld_status
                    # findic['rd'][int(i)] = rd_status
                    frame = cv2.putText(frame, "status : {}".format(ld_status), (80, 80),
                                cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,255,0 if (ld_status == "Closed") else 255), 2)
                    # frame = cv2.putText(frame, "right door : {}".format(rd_status), (400, 80),
                    #             cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,255,0 if (rd_status == "Closed") else 255), 2)
                    cv2.imshow('video',frame)
                key = cv2.waitKey(0)


                # print(ld_dict,'----------->',rd_dict)
                # check if 'space' is pressed and resume playing
                if (key & 0xFF == 32):
                    break

                # check if 'left arrow' is pressed and rewind video to the previous frame, but do not play
                if (key & 0xFF == ord('a')):
                    cur_frame_number = i #video_caputre.get(cv2.CAP_PROP_POS_FRAMES)
                    print('* At frame #' + str(cur_frame_number))

                    prev_frame = cur_frame_number
                    if (cur_frame_number > 0):
                        prev_frame -= 1

                    print('* Rewind to frame #' + str(prev_frame))
                    video_caputre.set(cv2.CAP_PROP_POS_FRAMES, prev_frame)

                # check if 'right arrow' is pressed and rewind video to frame 0, then resume playing
                if (key & 0xFF == ord('d')):
                    # video_caputre.set(cv2.CAP_PROP_POS_FRAMES, 0)
                    # break
                    cur_frame_number = i #video_caputre.get(cv2.CAP_PROP_POS_FRAMES)
                    print('* At frame #' + str(cur_frame_number))

                    prev_frame = cur_frame_number
                    if (cur_frame_number >= 0):
                        prev_frame += 1
                    print(prev_frame)
                    # if (prev_frame in findic['ld']) and (prev_frame in findic['rd']) :
                    #     print('inside if ',prev_frame)
                    #     ld_status = findic['ld'][int(prev_frame)]
                    #     rd_status = findic['rd'][int(prev_frame)]
                        
                    # else:

                    for sk in range(int(cur_frame_number),int(prev_frame)+1):
                        # if sk % 5 == 0:
                            # print(sk)
                        findic['ld'][int(i)] = ld_status

                            # findic['rd'][int(i)] = rd_status

                    print('* Forward to frame #' + str(prev_frame))
                    video_caputre.set(cv2.CAP_PROP_POS_FRAMES, prev_frame)
                    

                if (key & 0xFF == ord('e')):
                    lsw = (lsw + 1) % 3
                    print('e pressed ',lsw,status[lsw])
                    video_caputre.set(cv2.CAP_PROP_POS_FRAMES,i)
                    
                # if (key & 0xFF == ord('r')):
                #     rsw = (rsw + 1) % 2
                #     print('r pressed',rsw,status[rsw])

                
        # exit when 'q' is pressed to quit
        if (key & 0xFF == ord('q')):
            break
    
    # response = input("save annotations ? , Press 'y' to save or 'n' to exit ?")
    # print(response)
    # if response == 'y':
    tdf = pd.DataFrame(findic) #lddf.append(rddf)
    tdf.to_csv(args.output_path+path.split('/')[-1]+'_status.csv')   #/home/shyam/bridge_tech/Archive/GT/
    # else:
    #     print('Exiting !')
    #     exit(0)
    print(tdf.head())
    # print((tdf['ld']=='visible').idxmax())
    tdf['vnv'] = np.where(tdf['ld'].isin(['visible','not visible']),1,0)
    tdf['vis'] = np.where(tdf['ld'].isin(['visible']),1,0)

    s = tdf.vnv.ne(1)

    # groupby
    vnv_df = tdf.groupby([s, s.cumsum()]).apply(lambda x: [len(list(x.index)),min(list(x.index)),max(list(x.index))])
    print('newdf',vnv_df)
    vnv = vnv_df.loc[False].apply(pd.Series)
    vnv.rename(columns={0:'total_frames',1:'tstart_frame',2:'tend_frame'},inplace=True)
    vnv = vnv.reset_index(drop=True)
    print(vnv)

    d = tdf.vis.ne(1)

    # groupby
    vis_df = tdf.groupby([d, d.cumsum()]).apply(lambda x: [len(list(x.index)),min(list(x.index)),max(list(x.index))])
    vis = vis_df.loc[False].apply(pd.Series)
    vis.rename(columns={0:'visible_frames',1:'vstart_frame',2:'vend_frame'},inplace=True)
    vis = vis.reset_index(drop=True)
    print(vis)

    dirdf = pd.concat([vis,vnv],axis=1)
    # dirdf = dirdf.reset_index(drop=True)
    
    dirdf['video'] = [name]*len(vis)
    dirdf['folder'] = [path.split('/')[-2]]*len(vis)
    print(dirdf,dirdf.shape)

    # dirdf = pd.DataFrame()
    # dirdf = pd.DataFrame([vis,vnv],index=['visible','total'],columns=[[name]*len(vis),[path.split('/')[-2]*len(vis)]])
    # dirdf = dirdf.T
    
    # values, counts = np.unique(findic.values(), return_counts=True)
    # cntdic = tdf['ld'].value_counts().to_dict()
    # cntdf = pd.DataFrame({name:cntdic})
    # cntdf = cntdf.T
    # cntdf['frame_count'] = frame_count
    # # cntdic = cntdf.to_dict()
    # cntdf = cntdf.reset_index()
    # # tagdf = tagdf.reset_index()
    tagdf = tagdf.append(dirdf)
    print(dirdf)
    print(tagdf)
    tagdf.to_csv(args.output_path+'tag_view_count.csv',index=True)

    # release resources
    video_caputre.release()
    cv2.destroyAllWindows()


def main(args):
    path = args.input_file #"/home/shyam/bridge_tech/Archive/02-06-2021_4757_14258/"+vid[0] #02-06-2021_16-09-44.avi"
    print(path)
    video_caputre = cv.VideoCapture(path)

        # Get the parameters of the imported video
    fps = video_caputre.get(cv.CAP_PROP_FPS)
    width = video_caputre.get(cv.CAP_PROP_FRAME_WIDTH)
    height = video_caputre.get(cv.CAP_PROP_FRAME_HEIGHT)

    print("fps:", fps)
    print("width:", width)
    print("height:", height)

    # if os.path.exists(args.output_path+path.split('/')[-1]+'_gt.csv'):
    #     df = pd.read_csv(args.output_path+path.split('/')[-1]+'_gt.csv')
    #     # df = df.iloc[:,1:]
    #     # vid = args.input_file
    #     vid = cv2.VideoCapture(args.input_file)
    #     frame_count = int(vid.get(cv2.CAP_PROP_FRAME_COUNT))
    #     # print(df.iloc[:,0])
    #     print(frame_count-(frame_count%5),max(df.iloc[:,0]))
    #     frame_count = frame_count - 1
    #     if max(df.iloc[:,0]) == frame_count-(frame_count%5):
    #         response = input("Csv file already exists , Press 'y' to review or 'n' to exit ?")
    #         print(response)
    #         if response == 'y':
    #             csv_path = args.output_path+path.split('/')[-1]+'_gt.csv'
    #             review(path,csv_path)
    #         else:
    #             print('Exiting !')
    #             exit(1)
    #     else:
    #         print('Incomplete annotations')
    # else:
    annotate(path,video_caputre)

    

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    # Required arguments.
    parser.add_argument(
        "-i",
        "--input_file",
        required=True,
        help="Path to directory containing video file")
    parser.add_argument(
            "-o",
            "--output_path",
            required=True,
            help="Path to store the annotation csv file")

    args = parser.parse_args()
    main(args)

       
       
       
       
       
